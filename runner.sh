#!/bin/bash
export DIR_TRUMON=/home/$USER/dev
# This use to recv 1
$DIR_TRUMON/auto-start/Server_Application/recv1.sh &&
# this use to recv 2
$DIR_TRUMON/auto-start/Server_Application/recv2.sh &&
# this use to imagerecon
$DIR_TRUMON/auto-start/Server_Application/imagerecon.sh &&
# This use to parser
$DIR_TRUMON/auto-start/Server_Application/parser.sh &&
echo "===============================" &&
echo "|| Application Started >>>>  ||" &&
echo "===============================" ||
echo "Error, please set DIR_TRUMON Properly"
